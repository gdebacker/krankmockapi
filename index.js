var data = require('./src/data');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

/**
 * Mock Server creation and routes creation
 */

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('assets'));

var port = process.env.PORT || 1080;

var router = express.Router();

/* GET MATCHES PER DAYS */
router.get('/matchs', function(req, res) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.json( data.getMatchsPerDays() );
});

/* GET ALL INFOS OF ONE USER */
router.get('/users/:userid', function(req, res) {
    var userid = req.params.userid;
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.json( data.getUsersById(userid) );
});

app.use('/api', router);

app.listen(port);