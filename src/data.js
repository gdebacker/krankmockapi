var matchModel = require('./models/match');
var userModel = require('./models/user');
var moment = require('moment');
var Match =  matchModel.createMatchModel();
var User =  userModel.createUserModel();

/**
 * The purpose of this module is to simulate a very simple data service with sample mock data
 */

var user1 = User.create();
user1.id(1);
user1.firstname('Christophe');
user1.lastname('N\'Guyen');
user1.favoritesDays([0,1,2]);
user1.sports([
    {
        'name': 'football',
        'note': '7.2',
        'organized': '5',
        'played': '10'
    },
    {
        'name': 'running',
        'note': '5',
        'organized': '2',
        'played': '5'
    },
    ]
);
user1.favoritesSports([ {
    'name': 'football',
    'note': '7.2',
    'organized': '5',
    'played': '10'
}]);
user1.thumbnail('http://localhost:1080/images/users/1.jpg');

var user2 = User.create();
user2.id(2);
user2.firstname('Tedd');
user2.lastname('Niels');
user2.favoritesDays([3,4,5]);
user2.sports([
        {
            'name': 'football',
            'note': '7.2',
            'organized': '5',
            'played': '10'
        },
        {
            'name': 'running',
            'note': '5',
            'organized': '2',
            'played': '5'
        },
    ]
);
user2.favoritesSports([ {
    'name': 'football',
    'note': '7.2',
    'organized': '5',
    'played': '10'
}]);
user2.thumbnail('http://localhost:1080/images/users/1.jpg');
user2.partners([user1.toJSON()]);
user1.partners([user2.toJSON()]);

var users = [user1,user2];

var match1 = Match.create();
match1.id(1);
match1.type("football");
match1.title('Football Soccer Live Lalala');
match1.date(moment('06/11/2018 8:30').format('dddd DD MMMM'));
match1.hour(moment('06/11/2018 8:30').format('hh:mm A'));
match1.day(moment('06/11/2018 8:30').format('ddd.DD'));
match1.userId(user1.id());
match1.user(JSON.parse(JSON.stringify(users.filter(item => item.id() === match1.userId())[0])));
match1.cost('8');
match1.duration('1:30');
match1.matchTags(['5vs5','Expert']);
match1.full(false);

var match2 = Match.create();
match2.id(2);
match2.type("running");
match2.title('Running - Canal de l\'Ourq');
match2.date(moment('06/12/2018 9:30').format('dddd DD MMMM'));
match2.hour(moment('06/12/2018 9:30').format('hh:mm A'));
match2.day(moment('06/12/2018 9:30').format('ddd.DD'));
match2.userId(user2.id())
match2.user(JSON.parse(JSON.stringify(users.filter(item => item.id() === match2.userId())[0])));
match2.cost('8');
match2.duration('1:30');
match2.matchTags(['5vs5','Expert']);
match2.full(true);

var match3 = Match.create();
match3.id(3);
match3.type("running");
match3.title('Running - Autre course');
match3.date(moment('06/12/2018 17:30').format('dddd DD MMMM'));
match3.hour(moment('06/12/2018 17:30').format('hh:mm A'));
match3.day(moment('06/12/2018 17:30').format('ddd.DD'));
match3.userId(user2.id());
match3.user(JSON.parse(JSON.stringify(users.filter(item => item.id() === match3.userId())[0])));
match3.cost('8');
match3.duration('1:30');
match3.matchTags(['5vs5','Expert']);
match3.full(true);

var matchs = [match1.toJSON(), match2.toJSON(), match3.toJSON()];

//Work with days and match objects to create a match list with day keys
var days = [...new Set(matchs.map(item => item.date))];
var matchsPerDays = {};
days.forEach(function(day,index) {
    matchsPerDays[index] = {};
    matchsPerDays[index]["day"] = day;
    matchsPerDays[index]["matchs"] = matchs.filter(item => item.date === day);
});

/* Service methods returning users, matches, etc. */
module.exports = {
    getMatchs: function() {
        return matchs;
    },
    getMatchsPerDays: function(){
        console.log(matchsPerDays);
        return matchsPerDays;
    },
    getUsers: function() {
        return users;
    },
    getUsersById: function(userId){
        return users.find(function(user) {
            return user.id() == userId;
        });
    }

};