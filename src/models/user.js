var model = require('nodejs-model');

/**
 * User object model
 *  id
 *  firstname
 *  lastname
 *  matchPlayedCount
 *  matchOrganizedCound
 *  favoritesDays
 *  sports
 *  favoritesSports
 *  partners
 */

module.exports = {
    createUserModel: function() {

        var User = model("User").attr('id', {
            validations: {
                presence: {
                    message: 'id is required!'
                }
            }
        }).attr('firstname', {
            validations: {
                presence: {
                    message: 'firstname is required!'
                }
            }
        }).attr('lastname', {
            validations: {
                presence: {
                    message: 'lastname is required!'
                }
            }
        }).attr('matchPlayedCount', {
            validations: {
            }
        }).attr('matchOrganizedCount', {
            validations: {
            }
        }).attr('favoritesDays', {
            validations: {
            }
        }).attr('sports', {
            validations: {
                presence: {
                    message: 'firstname is required!'
                }
            }
        }).attr('favoritesSports', {
            validations: {
            }
        }).attr('partners', {
            validations: {
            }
        }).attr('thumbnail', {
                validations: {
                }
            });

        return User;

    }

};