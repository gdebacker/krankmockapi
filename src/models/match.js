var model = require('nodejs-model');

/**
 * Match object model
 *  id
 *  title
 *  date
 *  hour
 *  userId
 *  cost
 *  duration
 *  matchTags
 */

module.exports = {
    createMatchModel: function() {

        var Match = model("Match").attr('id', {
            validations: {
                presence: {
                    message: 'id is required!'
                }
            }
        }).attr('type', {
            validations: {
                presence: {
                    message: 'title is required!'
                }
            }
        }).attr('title', {
            validations: {
                presence: {
                    message: 'title is required!'
                }
            }
        }).attr('date', {
            validations: {
                presence: {
                    message: 'date is required!'
                }
            }
        }).attr('day', {
            validations: {
            }
        }).attr('hour', {
            validations: {
            }
        }).attr('userId', {
            validations: {
                presence: {
                    message: 'user id is required!'
                }
            }
        }).attr('user', {
            validations: {
                presence: {
                    message: 'user id is required!'
                }
            }
        }).attr('cost', {
            validations: {
            }
        }).attr('duration', {
            validations: {
            }
        }).attr('matchTags', {
            validations: {
            }
        }).attr('full', {
            validations: {
            }
        });

        return Match;

    }

};