### Installation
1. npm install
2. npm start
3. Ignore the warning and enjoy

### Description
A very simple mock API not connected to any database. Only objects created on the fly are consumed by the API and returned to the user.

### Folders
- src
 - assets
 - models ( *data models* )
   - user.js
   - match.js
 - data.js  ( *data service* )
- index.js ( *webservice... * )